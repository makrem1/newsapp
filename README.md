# News APP iOS

Ce projet est un exemple de la façon dont vous pouvez diviser un programme Swift iOS en deux composants distincts pour l'interface utilisateur (UI) et l'API.

## Pourquoi diviser le programme en deux composants ?

Diviser le programme en deux composants distincts (UI et API) peut offrir plusieurs avantages :

- Une meilleure organisation et une meilleure maintenance du code.
- Une meilleure extensibilité et réutilisabilité de l'application.
- Des performances améliorées en gérant les requêtes et les réponses de l'API en arrière-plan.

## Comment cela fonctionne-t-il ?

Le composant UI contient tout le code lié à l'interface utilisateur de l'application, y compris les contrôleurs de vue, les vues et les fichiers de ressources. Le composant API contient tout le code lié à la logique de l'API, y compris les appels de réseau, la gestion des erreurs et la désérialisation des données.

En divisant l'application de cette manière, il est plus facile de gérer et de maintenir le code, et de le rendre plus facilement extensible et réutilisable.



## Configuration

Pour utiliser cette application, **vous devez créer un fichier** `Configuration.plist` dans le projet avec deux clés :

- `base URL` : l'URL de base de l'API NewsAPI. Vous pouvez obtenir cette URL sur le site Web de NewsAPI.
- `API Key` : votre clé d'API NewsAPI. Vous pouvez obtenir votre clé API sur le site Web de NewsAPI.

Voici comment créer le fichier `Configuration.plist` et ajouter les clés :

1. Dans le projet Xcode, cliquez avec le bouton droit sur le dossier de votre projet et sélectionnez "New File".
2. Sélectionnez "Property List" dans la catégorie "Resource" et cliquez sur "Next".
3. Nommez le fichier "Configuration.plist" et enregistrez-le dans le dossier racine de votre projet.
4. Dans le fichier `Configuration.plist`, ajoutez deux entrées sous forme de clé-valeur :
   - `baseURL` : avec la valeur de l'URL de base de l'API NewsAPI. (https://newsapi.org/v2)
   - `apiKey` : avec la valeur de votre clé API NewsAPI.

Vous pouvez maintenant utiliser ces valeurs dans votre application pour accéder à l'API NewsAPI.


