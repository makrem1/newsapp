//
//  NewsViewModel.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation
import NewsAPI

typealias Article = Models.Response.Article
typealias News = Models.Response.NewsAPIResponse


enum DataStatus {
    case initial
    case loading
    case paging
    case success
    case failure(Error)
}

class NewsViewModel: ObservableObject {
    
    func newSearch() {
        self.page = 0
        self.articleList.removeAll()
        self.isLoaded = false
        self.hasMoreItems = false
//        self.hasMoreItems = false
    }
    var isLoaded: Bool {
        get {
            return !articleList.isEmpty
        }
        set {
        }
    }

    var isInitial: Bool = true
    var hasMoreItems: Bool {
        get {
            let totalArticles = articleList.count
            let lastPageIndex = (totalArticles / self.perpage) + (totalArticles % self.perpage > 0 ? 1 : 0)
            return lastPageIndex > self.page
        }
        set {
        }
    }
    
    @Published var articleList: [Article] = []
    @Published var articles: News?
    let api = Manager.shared
    var status: DataStatus = .initial
    var page = 0
    var perpage = 5
    let categories = ["General", "Sports", "Health", "Business", "Technology", "Science", "Entertainment"]
    
    /*func getTopics(category: String) {
        api.topHeadLines(country: "us", category: category)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Error: \(error)")
                    self.status = .failure(error)
                    self.isInitial = false
                case .finished:
                    break
                }
            }, receiveValue: { result in
                switch result {
                case .success(let response):
                    self.isInitial = false
                    print(response.articles.map { $0 })
                    self.status = .success
                    self.articles = response
                    self.articleList = response.articles
                case .failure(let error):
                    self.isInitial = false
                    print("Error: \(error)")
                    self.status = .failure(error)
                }
            })
            .store(in: &api.cancellables)
        
    }*/
    
    func getTopics(category: String) {
        print("ßßßßß \(category)")
        status = page == 1 ? .loading : .paging
        api.topHeadLines(country: "us", category: category, page: self.page + 1, pageSize: self.perpage)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Error: \(error)")
                    self.status = .failure(error)
                case .finished:
                    break
                }
            }, receiveValue: { result in
                switch result {
                case .success(let response):
                    self.status = .success
                    self.page += 1
                    self.articleList.append(contentsOf: response.articles)
                case .failure(let error):
                    print("Error: \(error)")
                    self.status = .failure(error)
                }
            })
            .store(in: &api.cancellables)
    }
    
    
    
    func getNews(key: String) {
        status = page == 1 ? .loading : .paging
        api.allNews(key: key, page: self.page + 1, pageSize: self.perpage)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Error: \(error)")
                    self.status = .failure(error)
                case .finished:
                    break
                }
            }, receiveValue: { result in
                switch result {
                case .success(let response):
                    self.status = .success
                    self.page += 1
                    self.articleList.append(contentsOf: response.articles)
                case .failure(let error):
                    print("Error: \(error)")
                    self.status = .failure(error)
                }
            })
            .store(in: &api.cancellables)
        
    }
}
