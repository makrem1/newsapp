//
//  WebViewConfig.swift
//  NewsApp
//
//  Created by Makrem Chambah on 16/4/2023.
//

import Foundation 
import SwiftUI
import SafariServices


struct WebViewDetails: UIViewControllerRepresentable {
    
    let url: URL
    
    func makeUIViewController(context: Context) -> some SFSafariViewController {
        SFSafariViewController(url: url)
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {}
    
}
