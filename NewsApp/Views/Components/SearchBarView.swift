//
//  SearchBarView.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

import SwiftUI

struct SearchBarView: View {
    @Binding var searchQuery: String
    var onSearchTapped: () -> Void
    
    var body: some View {
        HStack {
            TextField("Chercher", text: $searchQuery, onCommit: onSearchTapped)
                .padding(.horizontal, 10)
            Button(action: onSearchTapped) {
                Text("Chercher")
            }
            .padding(.trailing, 10)
        }
        .frame(height: 44)
        .background(Color.gray.opacity(0.2))
        .cornerRadius(10)
        .padding(.horizontal, 10)
    }
}
