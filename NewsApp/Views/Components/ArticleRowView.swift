//
//  ArticleRowView.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation
import SwiftUI
import NewsAPI


struct ArticleRow: View {
    let article: Article
    
    var body: some View {
        VStack(alignment: .center, spacing: 5) {
            NetworkImage(url: article.urlToImage)
            Group {
                if let title = article.title {
                    Text(title)
                        .font(.headline)
                        .lineLimit(3)
                }
                
                if let description = article.description {
                    Text(description)
                        .foregroundColor(.gray)
                        .lineLimit(4)
                        .font(.system(size: 12))
                }
            }
            .padding([.horizontal, .bottom])
        }
    }
}


struct NetworkImage: View {
    let url: URL?
    
    var body: some View {
        AsyncImage(
            url: url,
            content: { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
            },
            placeholder: {
                Group {
                    if url != nil {
                        VStack {
                            Spacer()
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle())
                            Spacer()
                        }
                    } else {
                        HStack {
                            Spacer()
                            Image("NotFound")
                                .resizable()
                                .cornerRadius(10)
                                .aspectRatio(contentMode: .fill)
                            Spacer()
                        }
                    }
                }.frame(alignment: .center)
            }
        )
        
        .frame(minHeight: 200, maxHeight: 300)
        .cornerRadius(10)
        .clipped()
    }
}
