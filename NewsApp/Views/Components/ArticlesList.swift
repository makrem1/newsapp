//
//  ArticlesList.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

import SwiftUI
/*
struct ArticlesList: View {
    @StateObject private var viewModel = NewsViewModel()
    @Binding var selectedCategory: String
    
    var body: some View {
        VStack {
            switch viewModel.status {
            case .initial:
                ProgressView()
            case .loading:
                ProgressView()
            case .success:
                if let articles = viewModel.articles {
                    List(articles.articles, id: \.self) { article in
                        NavigationLink(destination: ArticleDetailView(article: article)) {
                            ArticleRow(article: article)
                        }
                        .listRowSeparator(.hidden)
                    }
                    .listStyle(.plain)
                }
            case .failure(let error):
                Text("Error: \(error.localizedDescription)")
            case .paging:
                ProgressView()
            }
        }.onAppear() {
            print("selected = ", self.selectedCategory)
            self.viewModel.getTopics(category: self.selectedCategory)
        }
        .onChange(of: selectedCategory) { newCategory in
            self.viewModel.getTopics(category: newCategory)
        }
    }
}
*/
