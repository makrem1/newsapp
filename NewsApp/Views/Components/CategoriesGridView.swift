//
//  CategoriesGridView.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation
import SwiftUI


struct CategoriesGridView: View {
    @StateObject private var viewModel = NewsViewModel()
    @Binding var selectedCategory: String
    var onCategoryTapped: (String) -> Void
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHGrid(rows: [GridItem(.flexible())]) {
                ForEach(viewModel.categories, id: \.self) { category in
                    Button(action: {
                        onCategoryTapped(category)
                        self.selectedCategory = category
                    }) {
                        Text(category)
                            .foregroundColor(selectedCategory == category ? Color.white : Color.black)
                            .padding(10)
                            .background(selectedCategory == category ? Color.black : Color.white)
                            .cornerRadius(10)
                            
                    }
                    .padding(.horizontal, 5)
                }
            }
        }
        .frame(height: 50)
    }
}
