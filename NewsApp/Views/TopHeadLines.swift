//
//  TopHeadLines.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

 import SwiftUI

struct HeadlinesView: View {
    @State private var selectedCategory = "General"
    
    var body: some View {
        NavigationView {
            VStack(spacing: 5) {
                
                TopHeadLinesListView(selectedCategory: $selectedCategory)
                    .navigationBarTitle(Consts.titles.TOP_HEADLINES_VIEW)
                Spacer()
            }
        }
    }
}




struct TopHeadLinesListView: View {
    @StateObject private var viewModel = NewsViewModel()
    @Binding var selectedCategory: String
    
    var body: some View {
        VStack(spacing: 5) {
            CategoriesGridView(selectedCategory: $selectedCategory, onCategoryTapped: { category in
                viewModel.newSearch()
                self.viewModel.getTopics(category: category)
            })
            
            
            switch viewModel.status {
            case .initial:
                ProgressView().foregroundColor(.black)
                
            case  .success:
                if viewModel.articleList.isEmpty {
                    Text(Consts.Labels.SEARCH_NOT_FOUND)
                } else {
                    listNewsView
                }
                
            case .failure(let error):
                Text("Error: \(error.localizedDescription)")
                
            case .loading:
                ProgressView()
                
            case .paging:
                ProgressView()
            }
        }.onAppear() {
            print("selected = ", self.selectedCategory)
            self.viewModel.getTopics(category: self.selectedCategory)
        }
    }
    
    private var listNewsView: some View {
        List {
            ForEach(viewModel.articleList, id: \.self) { element in
                NavigationLink(destination: ArticleDetailView(article: element)) {
                    
                    ArticleRow(article: element)
                        .fixedSize(horizontal: false, vertical: true)
                        .frame(width: 300)
                }
            }
            if !viewModel.hasMoreItems {
                ProgressView()
                    .onAppear {
                        viewModel.getTopics(category: selectedCategory)
                    }
            }
        }.listStyle(PlainListStyle())
    }
}

