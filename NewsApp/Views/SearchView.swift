//
//  SearchView.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

import SwiftUI
import Foundation

struct SearchView: View {
    @State private var searchQuery = ""
    @StateObject private var viewModel = NewsViewModel()

    var body: some View {
        NavigationView {
            VStack {
                searchListView
            }
            .navigationBarTitle(Consts.titles.SEARCH_VIEW)
        }
    }

    private var searchListView: some View {
        VStack {
            SearchBarView(searchQuery: $searchQuery, onSearchTapped: {
                print(22)
                viewModel.newSearch()
                viewModel.getNews(key: searchQuery)
                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)

            })

            switch viewModel.status {
            case .initial:
                Text(Consts.Labels.SEARCH_INITIAL)
            case .loading:
                ProgressView()
            case .failure(let error):
                VStack (spacing: 10){
                    Text(Consts.Labels.SEARCH_ERROR)
                    Text(error.localizedDescription)
                }
                
            case .success:
                if viewModel.articleList.isEmpty {
                    Text(Consts.Labels.SEARCH_NOT_FOUND)
                } else {
                    listNewsView
                }
            case .paging:
                ProgressView()
            }
            Spacer()
        }
    }

    private var listNewsView: some View {
        List {
            ForEach(viewModel.articleList, id: \.self) { element in
                NavigationLink(destination: ArticleDetailView(article: element)) {
                    ArticleRow(article: element)
                        .fixedSize(horizontal: false, vertical: true)
                        .frame(maxWidth: .infinity)
                }
            }
            if !viewModel.hasMoreItems {
                ProgressView()
                    .onAppear {
                        viewModel.getNews(key: searchQuery)
                    }
            }
        }.listStyle(GroupedListStyle())
    }
}
