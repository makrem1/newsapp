//
//  HomeView.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation
import SwiftUI

struct HomeView: View {
    var body: some View {
        TabView {
            HeadlinesView()
                .tabItem {
                    Label(Consts.titles.TOP_HEADLINES_TAB, systemImage: Consts.SysIcon.NEWSPAPER_TAB)
                }
            
            SearchView()
                .tabItem {
                    Label(Consts.titles.SEARCH_VIEW, systemImage: Consts.SysIcon.SEARCH_TAB)
                }
        }
    }
}
