//
//  DetailsArticleView.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation
import NewsAPI
import SwiftUI


struct ArticleDetailView: View {
    let article: Article
    @State private var showFullDescription = false
    
    var body: some View {
            VStack {
                WebViewDetails(url: article.url!)
                    .navigationBarTitle(Text(article.title!), displayMode: .inline)
                    .navigationBarItems(trailing: ShareButton(url: article.url!))
            }
        }
}

struct ShareButton: View {
    let url: URL
    
    var body: some View {
        Button(action: share) {
            Image(systemName: Consts.SysIcon.SHARE_BUTTON)
        }
    }
    
    private func share() {
        let av = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
              let keyWindow = windowScene.windows.first(where: { $0.isKeyWindow }),
              let rootViewController = keyWindow.rootViewController else {
                  return
              }
        rootViewController.present(av, animated: true, completion: nil)
    }
}
