//
//  Consts.swift
//  NewsApp
//
//  Created by Makrem Chambah on 21/4/2023.
//

import Foundation
import SwiftUI

class Consts {
    
    enum titles {
        static var SEARCH_VIEW        = "Recherche"
        static var TOP_HEADLINES_TAB  = "Actualité"
        static var TOP_HEADLINES_VIEW = "Top headlines"
    }
    
    enum Labels {
        static var SEARCH_ERROR     = "Erreur lors du chargement"
        static var SEARCH_INITIAL   = "Taper le texte pour commencer !"
        static var SEARCH_NOT_FOUND = "Aucun article trouvé"
    }
    
    enum SysIcon {
        static var SEARCH_TAB    = "magnifyingglass"
        static var SHARE_BUTTON  = "square.and.arrow.up"
        static var NEWSPAPER_TAB = "newspaper" 
    }
    
    
}
