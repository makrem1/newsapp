//
//  NewsAppApp.swift
//  NewsApp
//
//  Created by Makrem Chambah on 15/4/2023.
//

import SwiftUI

@main
struct NewsAppApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
